module github.com/azhai/xgen

go 1.20

require (
	github.com/arriqaaq/flashdb v0.1.6
	github.com/azhai/gozzo v1.3.8
	github.com/go-playground/form/v4 v4.2.0
	github.com/go-redis/redis/v8 v8.11.5
	github.com/go-sql-driver/mysql v1.7.1
	github.com/gobwas/glob v0.2.3
	github.com/gomodule/redigo v1.8.9
	github.com/grsmv/inflect v0.0.0-20140723132642-a28d3de3b3ad
	github.com/hashicorp/hcl/v2 v2.17.0
	github.com/json-iterator/go v1.1.12
	github.com/k0kubun/pp v2.4.0+incompatible
	github.com/klauspost/cpuid/v2 v2.2.5
	github.com/lib/pq v1.10.9
	github.com/manifoldco/promptui v0.9.0
	github.com/mattn/go-sqlite3 v1.14.17
	github.com/mitchellh/copystructure v1.2.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.8.4
	go.uber.org/zap v1.24.0
	golang.org/x/tools v0.11.0
	xorm.io/xorm v1.3.2
)

require (
	github.com/agext/levenshtein v1.2.3 // indirect
	github.com/apparentlymart/go-textseg/v13 v13.0.0 // indirect
	github.com/arriqaaq/aol v0.1.2 // indirect
	github.com/arriqaaq/art v0.1.2 // indirect
	github.com/arriqaaq/hash v0.1.2 // indirect
	github.com/arriqaaq/set v0.1.2 // indirect
	github.com/arriqaaq/zset v0.1.2 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/chzyer/readline v1.5.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/goccy/go-json v0.10.2 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/go-cmp v0.5.3 // indirect
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/mitchellh/go-wordwrap v1.0.1 // indirect
	github.com/mitchellh/reflectwalk v1.0.2 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/zclconf/go-cty v1.13.2 // indirect
	go.uber.org/atomic v1.11.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/mod v0.12.0 // indirect
	golang.org/x/sys v0.10.0 // indirect
	golang.org/x/text v0.11.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	xorm.io/builder v0.3.12 // indirect
)
